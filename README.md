# ITMIX2 - Ensemble approach

## Model description

Source codes for the model **GilletChaulet** in ITMIX2, the second phase of the Ice Thickness Models Intercomparison eXperiment.

The model is detailled in the ITMIX2 publication ([Farinotti et al. (2021)](https://www.frontiersin.org/articles/10.3389/feart.2020.571923/full)).

This approach takes model results submitted to [ITMIX1](https://tc.copernicus.org/articles/11/949/2017)  and apply an ensemble Kalman Filter to assimilate the calibration observations. For this we rely on the Parallel Data Assimilation Framework ([Nerger et al., 2005](http://pdaf.awi.de)).


## Running the ITMIX2 experiments

The subdirectory [*Codes*](./Codes) contains a suite of **bash**, **R**, and **Fortran** codes to process the ITMIX2 data-sets, the ITMIX results and run the assimilation.

To reproduce the ITMIX2 results:
1. Get the [required datasets](#-Required-datasets).
2. See [Installation](#-Installation) for details on the requirements to run the codes.
3. Edit the code **ITMIX2.sh** and update the environnement variables with the path to the datasets.
4. Run **ITMIX2.sh**. This will create a subdirectory in the **ITMIX2_WDIR** (update the value in **ITMIX2.sh**) for the given glacier experiment and run the assimilation for the 16 experiments (directories exp01 to exp16).
5. For each experiment **ENSEMBLE.nc** contains the updated ensemble with the variable **sim0** the ensemble mean. **Results.pdf** provides some statitics and plots of the initial and new estimate.  


## Required datasets

1. Download and unzip the ITMIX data sets and results available [here](http://dx.doi.org/10.5905/ethz-1007-92).
   - Unzip 01_ITMIX_input_data.zip and 02_ITMIX_results.zip
   - Edit the file *Fuerst_Synthetic1_bedrock.ascx* and **change the value of XLLCORNER from  150.62 to 150.625**.
   - Update **ITMIX_DIR=** in **ITMIX2.sh** with the path to your directory.

2. Download and unzip the ITMIX2 data sets available [here](https://drive.switch.ch/index.php/s/w9DXXwo3r3C2ouf).
   - Unzip 01_ITMIX2_input_data_and_instructions.zip.
   - Update **ITMIX2_DATA_DIR=** in **ITMIX2.sh** with the path to your directory.

> When you unzip the data sets keep the names for the directories as they are hard coded, i.e. the codes look to the directories *01_ITMIX_input_data* and *02_ITMIX_results* under **${ITMIX_DIR}** and *ITMIX2_input_data* under **${ITMIX2_DATA_DIR}**.

## Installation

- For the assimilation code:   
   1. Get and compile [PDAF](http://pdaf.awi.de). Version 1.13.1 was used to produce the results submitted to ITMIX2. 
   2. [Codes/EnDA](./Codes/EnDA) contains **fortran** codes that have been updated to run [PDAF in offine mode](http://pdaf.awi.de/trac/wiki/OfflineImplementationGuide) , edit the Makefile and compile the code. You need to have the fortran netcdf library.

- Pre- and Post- Processing:
   1. [gdal](https://gdal.org) is used to convert the ITMIX results from the .asc format to .gtiff.
   2. *R* codes are used for the pre- an post- processing. Here is the list of required R libraries:
      - rgdal
      - raster
      - rasterVis
      - ncdf4
      - splancs
      - ggplot2
      - cowplot
      - sf
      - scales
      - automap

## Notes

To produce the ITMIX2 results, the following three initial results from ITMIX did not estimate the thickness in the whole
glacier domain and where excluded from the initial ensemble:
  - Morlighem_Unteraar_bedrock.asc
  - Morlighem_Urumqi_bedrock.asc
  - Brinkerhoff-v2_Urumqi_bedrock.asc
