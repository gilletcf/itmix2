#!/bin/bash

###############################################################################
#* Run the ITMIX2 experiments for a given glacier
#* 
#* Authors: F. GILLET-CHAULET
#* Email:   fabien.gillet-chaulet@univ-grenoble-alpes.fr
#* Web:     https://gricad-gitlab.univ-grenoble-alpes.fr/gilletcf/itmix2
##############################################################################

# Name of the glacier experiment
## Chose in the list: Synthetic1 Unteraar Austfonna Synthetic2 SouthGlacier Freya Academy Synthetic3 Urumqi Starbuck
Glacier=Synthetic1

# Environnement variables:
## Update the following 3 variables (see README.md)

## Directory with ITMIX2 datasets:
export ITMIX2_DATA_DIR="/Path/To/01_ITMIX2_input_data_and_instructions"

## Directory with ITMIX datasets:
export ITMIX_DIR="/Path/To/ITMIX_dataset"

## Working directory: 
export ITMIX2_WDIR="/Path/To/Results/Dir"

#################################################
## Update the PATH
export PATH=$PATH:$PWD/Codes:$PWD/Codes/EnDA

###################################################
###################################################

## Pre-Processing:
##  create subdirectories and process calibration observations
echo 'Creating experiment for : ' $Glacier
ITMIX2_DESIGN.R $Glacier

## Pre-Processing:
##  get initial ensemble from ITMIX
echo 'Convert ITMIX .asc to .gtiff : '
read -p ' Press [Enter] key to continue...'
AscToGTiff.sh $Glacier

echo 'Create initial ensemble file : '
read -p ' Press [Enter] key to continue...'
ITMIX_PROCESSING.R $Glacier

## Assimilation: 
echo 'Run the assimilation : '
read -p ' Press [Enter] key to continue...'
Assimilation.sh $Glacier
