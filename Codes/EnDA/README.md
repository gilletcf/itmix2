## Ensemble Data Assimilation codes

The assimilation is based on [PDAF](http://pdaf.awi.de).

These codes have been adapted from the offline codes by F. Gillet-Chaulet (IGE,Grenoble)

The initial thickness ensemble is provided by a netcdf and the observations in an ascii text file.


### COMPILATION :
  - Get and compile PDAF.
  - Edit the **Makefile** and update the variables according to your environnement.

### INPUT :

Here are the main arguments that can be provided as command line arguments to the executable code *PDAF_offline*.
See sources codes **init_pdaf_parse.F90** and **init_pdaf_offline.F90** for the main arguments and their default values.


Here are the main arguments used in this example:

  - Initial ensemble stored in netcdf (**-infile** argument) using varnames *sim"i"* (i=1,dim_size)
    - we assume that the variables are store with the "y" dimension in decreasing order
     (what we get from the pre-processing the .gtiff)

  - Ensemble size  (**-dim_ens** argument)

  - Observation File (**-obsfile** argument):
    - observations provided in a text file as x,y,H^obs

  - Observation error (assumed uniform) (**-rms_obs** argument)

  - Other arguments (see PDAF for details, values under brackets are the values used for ITMIX2): 
     - **-filtertype** : type of filter (5 for LETKF)
     - **-local_range** : localisation radius (10 times the max ice thickness in the initial ensemble)
     - **-locweight** : localisation type (2 for 5th-order polynomial)

### OUPUT : 
  - The final analysed ensemble is saved as a netcdf (ENSEMBLE.nc) with *sim0* the ensemble mean
