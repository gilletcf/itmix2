!$Id: initialize.F90 1589 2015-06-12 11:57:58Z lnerger $
!BOP
!
! !ROUTINE: initialize  --- initialize the 2D offline example for PDAF
!
! !INTERFACE:
SUBROUTINE initialize()

! !DESCRIPTION:
! Routine to perform initialization of the 2D offline example for
! PDAF. Here, only the global size of the model domain and the
! global size of the model state vector need to be initialized.
! Generally, this could also be joined with the routine init_pdaf().
!
! For the 2D offline tutorial example, the domain is defined by the
! dimensions nx and ny. The state vector size is nx*ny.
!
! !REVISION HISTORY:
! 2013-02 - Lars Nerger - Initial code
! Later revisions - see svn log
!
! !USES:
  USE mod_assimilation, & ! Model variables
       ONLY: dim_state_p, nx, ny,xmin,ymin,dx,dy,infilename
  USE mod_parallel, &     ! Parallelization variables
       ONLY: MPI_COMM_WORLD, init_parallel, finalize_parallel

  USE NETCDF
  IMPLICIT NONE

  INTEGER :: NetCDFstatus
  INTEGER :: ncid,varid
  REAL,ALLOCATABLE,DIMENSION(:) :: x,y
!EOP
  NetCDFstatus = NF90_OPEN(trim(infilename),NF90_NOWRITE,ncid)
  IF ( NetCDFstatus /= NF90_NOERR ) THEN
     PRINT *,'Unable to open netcdf file',trim(infilename)
     PRINT *,'ABORT !!'
     STOP
  END IF
  
!Get x dim
  NetCDFstatus = nf90_inq_dimid(ncid, 'x', varid)
  IF ( NetCDFstatus /= NF90_NOERR ) THEN
     PRINT *,'Unable to get x-dim id'
     PRINT *,'ABORT !!'
     STOP
  ENDIF
  NetCDFstatus = nf90_inquire_dimension(ncid,varid,len=nx)
  IF ( NetCDFstatus /= NF90_NOERR ) THEN
     PRINT *,'Unable to get x-dim'
     PRINT *,'ABORT !!'
     STOP
  ENDIF


!Get y dim
  NetCDFstatus = nf90_inq_dimid(ncid, 'y', varid)
  IF ( NetCDFstatus /= NF90_NOERR ) THEN
     PRINT *,'Unable to get y-dim id'
     PRINT *,'ABORT !!'
     STOP
  ENDIF
  NetCDFstatus = nf90_inquire_dimension(ncid,varid,len=ny)
  IF ( NetCDFstatus /= NF90_NOERR ) THEN
     PRINT *,'Unable to get y-dim'
     PRINT *,'ABORT !!'
     STOP
  ENDIF

  allocate(x(nx),y(ny))
  NetCDFstatus = nf90_inq_varid(ncid,'x',varid)
     IF ( NetCDFstatus /= NF90_NOERR ) THEN
        PRINT *,'Unable to get var id for x'
        PRINT *,'ABORT !!'
        STOP
     ENDIF
  NetCDFstatus = nf90_get_var(ncid, varid,x)
  IF ( NetCDFstatus /= NF90_NOERR ) THEN
        PRINT *,'Unable to get var x'
        PRINT *,'ABORT !!'
        STOP
  ENDIF
  NetCDFstatus = nf90_inq_varid(ncid,'y',varid)
     IF ( NetCDFstatus /= NF90_NOERR ) THEN
        PRINT *,'Unable to get var id for y'
        PRINT *,'ABORT !!'
        STOP
     ENDIF
  NetCDFstatus = nf90_get_var(ncid, varid,y)
  IF ( NetCDFstatus /= NF90_NOERR ) THEN
        PRINT *,'Unable to get var y'
        PRINT *,'ABORT !!'
        STOP
  ENDIF
  
  xmin = x(1)
  ymin = y(ny)
  dx=(x(nx)-xmin)/(nx-1)
  dy=(y(1)-ymin)/(ny-1)
  xmin=xmin - dx/2.0
  ymin=ymin - dy/2.0

  NetCDFstatus = nf90_close(ncid)
  
! *** Model specifications ***
  dim_state_p   = nx * ny ! State dimension (shared via MOD_OFFLINE)

! *** Screen output ***
  WRITE (*, '(1x, a)') 'INITIALIZE MODEL INFORMATION FOR PDAF OFFLINE MODE'
  WRITE (*, '(22x,a)') 'MODEL: 2D Offline Example for Tutorial'
  WRITE (*, '(24x,a,i4,1x,a1,1x,i4)') 'Grid size:',nx,'x',ny
  WRITE (*, '(24x,a,f12.3,a,f12.3)') 'Grid dim: xllc :',xmin,' dx :',dx
  WRITE (*, '(24x,a,f12.3,a,f12.3)') 'Grid dim: yllc :',ymin,' dy :',dy
  WRITE (*, '(5x, a, i7)') &
       'Global model state dimension:', dim_state_p

! ****
  DEALLOCATE(x,y)
END SUBROUTINE initialize
