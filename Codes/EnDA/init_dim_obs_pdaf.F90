!$Id: init_dim_obs_pdaf.F90 1864 2017-12-20 19:53:30Z lnerger $
!BOP
!
! !ROUTINE: init_dim_obs_pdaf --- Compute number of observations
!
! !INTERFACE:
SUBROUTINE init_dim_obs_pdaf(step, dim_obs_p)

! !DESCRIPTION:
! User-supplied routine for PDAF.
! Used in the filters: SEEK/SEIK/EnKF/ETKF/ESTKF
!
! The routine is called at the beginning of each
! analysis step.  It has to initialize the size of 
! the observation vector according to the current 
! time step for the PE-local domain.
!
! Implementation for the 2D offline example
! without parallelization.
!
! !REVISION HISTORY:
! 2013-02 - Lars Nerger - Initial code
! Later revisions - see svn log
!
! !USES:
  USE mod_assimilation, &
       ONLY : obsfilename,nx, ny,xmin,ymin,dx,dy, obs_p, obs_index_p

  IMPLICIT NONE

! !ARGUMENTS:
  INTEGER, INTENT(in)  :: step       ! Current time step
  INTEGER, INTENT(out) :: dim_obs_p  ! Dimension of observation vector

! !CALLING SEQUENCE:
! Called by: PDAF_seek_analysis    (as U_init_dim_obs)
! Called by: PDAF_seik_analysis, PDAF_seik_analysis_newT
! Called by: PDAF_enkf_analysis_rlm, PDAF_enkf_analysis_rsm
! Called by: PDAF_etkf_analysis, PDAF_etkf_analysis_T
! Called by: PDAF_estkf_analysis, PDAF_estkf_analysis_fixed
!EOP

! *** Local variables
  INTEGER :: i                     ! Counters
  INTEGER :: cnt                   ! Counters
  INTEGER :: ok
  REAL :: x,y,h
  INTEGER :: xind,yind


! ****************************************
! *** Initialize observation dimension ***
! ****************************************
  OPEN (12, file=TRIM(obsfilename), status='old',iostat = ok)
  cnt = 0
  do while(ok == 0)
       read(12,*,iostat = ok)
       if (ok == 0) cnt = cnt + 1
  End do
  CLOSE (12)
  ! Set number of observations
  dim_obs_p = cnt

  ! Initialize vector of observations and index array
  IF (ALLOCATED(obs_index_p)) DEALLOCATE(obs_index_p)
  IF (ALLOCATED(obs_p)) DEALLOCATE(obs_p)
  ALLOCATE(obs_index_p(dim_obs_p))
  ALLOCATE(obs_p(dim_obs_p))

  OPEN (12, file=TRIM(obsfilename), status='old',iostat = ok)
  DO i = 1, cnt
       READ (12, *) x,y,h
       obs_p(i) = h

       ! get pixel indice
       xind=FLOOR((x-xmin)/dx)+1
       yind=FLOOR((y-ymin)/dy)+1
       obs_index_p(i)=(xind-1)*ny+yind
  END DO
  CLOSE (12)

! *** Clean up ***

END SUBROUTINE init_dim_obs_pdaf
