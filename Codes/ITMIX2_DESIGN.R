#!/usr/local/bin/Rscript 

###############################################################################
#* R code to prepare the ITMIX2 experiemental design
#* Usage:
#*  > ITMIX2_DESIGN.R glacier_Name
#*    where "glacier_Name" is one of the glacier in ITMIX and ITMIX2
#*
#* Output:
#*  - Create a sub_directory (glacier_Name) under the working directory 
#*     (set as ITMIX2_WDIR in the environnement)
#*  - Create sudirectories for each of the 16 experiments with 
#*     calibration and validation thicknesses 
#*  - Create a pdf (experiments.pdf)  with a figure of the 16 experiments 
#*     showing the galcier margin and the validation and calibration profiles
#*
#* Authors: F. GILLET-CHAULET
#* Email:   fabien.gillet-chaulet@univ-grenoble-alpes.fr
#* Web:     https://gricad-gitlab.univ-grenoble-alpes.fr/gilletcf/itmix2
##############################################################################

#################################################
#################################################
## Environnement variables
#################################################
# Directory with ITMIX2 data sets: 
#   - experiments key (ITMIX2_experiments_key.txt)
#   - Measured ice thickness (03_RES_*.txt)
itmix2 <- Sys.getenv("ITMIX2_DATA_DIR")

# Workind directory
wrdir  <- Sys.getenv("ITMIX2_WDIR")

#################################################
#################################################
## Required R libraries
#################################################
sPSM <- suppressPackageStartupMessages
sPSM(library(ggplot2))
sPSM(library(cowplot))
sPSM(library('sf'))

#################################################
#################################################
# FUNCTIONS
#################################################
#
#################################################
# Prepare the experiments
#################################################
GetGlacierObs <- function(glacier_Name) {

# Get experiment key from ITMIX2 data
  EkeysF <- file.path(itmix2,"ITMIX2_experiments_key.txt")
  Ekeys <- read.table(EkeysF, header=TRUE, quote="\"")

# Get data for the current glacier
  DataDir <- file.path(itmix2,"ITMIX2_input_data",glacier_Name)

# create directory to save processed measurements
  GlacierDir <- file.path(wrdir,glacier_Name)
  dir.create(GlacierDir)

# open experiment keys
 glacier <- subset(Ekeys, glacier==glacier_Name) 

# get measurements
  files <- list.files(DataDir,pattern ="03_RES_.*",full.names = TRUE)

  mymessage <- paste("  Get thickness data from :",files,sep = "")
  message(mymessage)

  thickness <-  read.table(files, header=TRUE, quote="\"")

# get margin
  shfiles <- list.files(paste(DataDir,"/shapefiles",sep=""),pattern =".shp$",full.names = TRUE)
  margin <- as.data.frame(st_coordinates(read_sf(shfiles)))

# loop through the 16  experiments
  nexp=16
  exp_names=sprintf("exp%02d", 1:nexp)
  
  p <- list()
  for (i in 1:nexp){
    ExpDir <- file.path(GlacierDir, exp_names[i])
    dir.create(ExpDir)
  
    cal_pNR <- glacier[ glacier[[exp_names[i]]] == 1 , ]
    # Get calibration and validation data sets
    calibration <- subset(thickness,pNR %in% cal_pNR$pNR,select = c("x_coord","y_coord","thick","pNR","dist"))
    validation <- subset(thickness,!pNR %in% cal_pNR$pNR,select = c("x_coord","y_coord","thick","pNR","dist"))
  
    write.table(calibration, file = file.path(ExpDir,"calibration.txt"), row.names = FALSE, sep=" ")
    write.table(validation, file = file.path(ExpDir,"validation.txt"), row.names = FALSE, sep=" ")

    p[[i]] <- ggplot(calibration,aes(x=x_coord,y=y_coord))  +
      geom_point(data=calibration,color="red",size=0.5) +
      geom_point(data=validation,color="black",size=0.5,alpha = 0.1) +
      geom_point(data=margin,aes(x=X,y=Y),color="blue",size=0.5) +
      theme(axis.text.x=element_blank(), axis.text.y=element_blank(),
            axis.title.x=element_blank(), axis.title.y=element_blank())
  }
  
  Figname <- file.path(GlacierDir,"experiments.pdf")
  p1 <- plot_grid(plotlist=p, labels = exp_names, ncol = 4)
  save_plot(Figname, p1,base_height=20,units = c("cm"))

  mymessage <- paste("  Experiment design saved in :",Figname,sep = "")
  message(mymessage)

}

#################################################
#################################################
# MAIN
#################################################
## arguments
args<-commandArgs(TRUE)
glacier_Name <- args[1]

message("########################################################")
mymessage <- paste("Create experiment design for glacier : ",glacier_Name,sep = "")
message(mymessage)
mymessage <- paste(" Working directory :",wrdir,sep = "")
message(mymessage)

GetGlacierObs(glacier_Name)

message("########################################################")
