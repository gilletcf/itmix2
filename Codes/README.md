## Source Codes

For a given glacier, the codes will be called in the following order:

1. **ITMIX2_DESIGN.R** *glacier* :
   - prepare the ITMIX2 experimental design
   - Use the following environnement variables:
     - ITMIX2_DATA_DIR
     - ITMIX2_WDIR 
 


2. **AscToGTiff.sh** *glacier* :
   - Convert ITMIX .asc results file to .gtiff using gdal
   - Use the following environnement variables:
     - ITMIX_DIR
     - ITMIX2_WDIR



3. **ITMIX_PROCESSING.R** *glacier* :
   - Create the initial ensemble file from ITMIX results
   - Use the following environnement variables:
     - ITMIX_DIR
     - ITMIX2_WDIR



4. **Assimilation.sh** *glacier* :
   - Run the 16 experiements 
     - EnDA/PDAF_offline is used for the assimilation. 
     - PostProcess.R generate some statistics and plots
   - Use the following environnement variables:
     - ITMIX2_WDIR
     - ITMIX2_DATA_DIR
