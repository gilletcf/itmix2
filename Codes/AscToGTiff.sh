#!/bin/bash

###############################################################################
## Use gdal to convert ITMIX .asc files to gtiff as R seems to have issues 
##  understanding NoDataValues in some cases
##  e.g. NODATA_VALUE is integer but after coded as float with trailing 0;
#*
#* Usage:
#*  > AscToGTiff.sh glacier_Name
#*    where "glacier_Name" is one of the glacier in ITMIX and ITMIX2
#* Inputs:
#*  - ITMIX results as .asc ${ITMIX_DIR}/02_ITMIX_results
#* 
#* Output:
#*  - ITMIX results as .gtiff under ${ITMIX2_WDIR}/"glacier_Name/ITMIX_ENS
#*
#* Authors: F. GILLET-CHAULET
#* Email:   fabien.gillet-chaulet@univ-grenoble-alpes.fr
#* Web:     https://gricad-gitlab.univ-grenoble-alpes.fr/gilletcf/itmix2
##############################################################################

########################################################
########################################################
# Input argumpent: name of the glacier
glacier=$1
echo '########################################################'
echo '########################################################'
echo ' AcsToGtiff: '
echo '   processing glacier: ' $glacier

# DIR with initial ITMIX results
RAW_DIR=${ITMIX_DIR}/02_ITMIX_results

# Ouput dir under ${ITMIX2_WDIR}/${glacier}
WDIR="${ITMIX2_WDIR}/${glacier}/ITMIX_ENS"
echo $WDIR
mkdir -p $WDIR


files=$(find $RAW_DIR -maxdepth 1 -type f -name "*_"$glacier"_bedrock.asc" | awk -F/ '{print $NF}')

#echo $files

for i in ${files[@]} ; do
  echo $i
  gdal_translate  -a_nodata -9999 "$RAW_DIR"/"$i" "${WDIR}/${i%.asc}.gtiff"
done

echo '########################################################'
echo '########################################################'
