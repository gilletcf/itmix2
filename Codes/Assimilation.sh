#!/bin/bash

###############################################################################
## Loop over the ITMIX2 16 experiements and assimilate the calibration observations.
##   Assimilation is done using the ensemeble data assimilation code PDAF
##    implemented in offline mode.
##   
#*
#* Authors: F. GILLET-CHAULET
#* Email:   fabien.gillet-chaulet@univ-grenoble-alpes.fr
#* Web:     https://gricad-gitlab.univ-grenoble-alpes.fr/gilletcf/itmix2
##############################################################################

################################################################################
## Usefull functions based on nco to read the NETCDF that contain the initial ensemble
## nc_functions: http://nco.sourceforge.net/nco.html
# ncmax $var_nm $fl_nm : What is maximum of variable?
function ncmax { ncwa -y max -O -C -v ${1} ${2} ~/foo.nc ; ncks --trd -H -C -v ${1} ~/foo.nc | cut -f 3- -d ' ' ; }
# ncvarlst $fl_nm : What variables are in file?
function ncvarlst { ncks --trd -m ${1} | grep -E ': type' | cut -f 1 -d ' ' | sed 's/://' | sort ; }
##
################################################################################

# Argument the name of the glacier
glacier_name=$1

# working directory
WDIR="${ITMIX2_WDIR}/${glacier_name}"

################################################################################
####  Parameters for the assimilation


## Ensmble size 
#   from sim* variables (minus 1 as sim0 is the esemble mean)
ENS_FILE="${WDIR}/ITMIX_ENS/ITMIX_${glacier_name}_.nc"
dim_size=$(ncvarlst $ENS_FILE | grep sim |  sed 's/^...//' |  sort -nr | head -n1 )

## Filter type
#   (5) LETKF
filtertype=5

## Range for localisation
Hrange=10.0
# Type of filter
local_range=$(echo $(ncmax sim0 $ENS_FILE) \* $Hrange  | bc -l)

## Localisation type
locweight=2

## Rms for the observations (assume uniform value)
rms_obs=5.0

echo
echo '#######################################'
echo '#######################################'
echo 'Processing glacier : ' $glacier_name
echo 'using ensemble file: ' $ENS_FILE
echo 'ensemble size: ' $dim_size
echo '#######################################'
echo 'filter: ' $filtertype
echo 'local range: ' $local_range '('$Hrange'*Hmax)'
echo '#######################################'
echo '#######################################'
echo

#
##
OBS_FILE=calibration.txt

## Loop through the 16 experiments

cd $WDIR

for i in $(seq -f "%02g" 1 16)
do
  echo exp"$i"
  # cd to exp dir
  cd exp"$i"

  ## process calibration data: x,y,H
  awk  '{if (NR>1) {print $1,$2,$3}}' $OBS_FILE > Hobs.txt

  ## run assimilation
  PDAF_offline -filtertype $filtertype -infile $ENS_FILE -dim_ens $dim_size -rms_obs $rms_obs -obsfile Hobs.txt -local_range $local_range -locweight $locweight > PDAF_"$i".out

  ## process results
  PostProcess.R $glacier_name  > outputFile.Rout 2> errorFile.Rout

  cd ..
done
